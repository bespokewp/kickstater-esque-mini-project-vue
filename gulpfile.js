////
// Dependencies
////
var
  gulp = require('gulp'),
  sass = require('gulp-ruby-sass'),
  autoprefixer = require('gulp-autoprefixer'),
  cssnano = require('gulp-cssnano'),
  jshint = require('gulp-jshint'),
  uglify = require('gulp-uglify'),
  rename = require('gulp-rename'),
  concat = require('gulp-concat'),
  del = require('del');


////
// Initial tasks (after npm install is successful)
////
gulp.task('project-start', function() {

  // move font files from @fortawesome package to usable folder
  gulp.src('node_modules/@fortawesome/fontawesome-free/webfonts/*')
    .pipe(gulp.dest('public/assets/webfonts'));

});


////
// Frontend styles
////
gulp.task('frontend-styles', function() {

  return sass('resources/scss/style.scss', { style: 'expanded' })
    .pipe(autoprefixer('last 2 version'))
    .pipe(gulp.dest('public/assets/css'))
    .pipe(rename({suffix: '.min'}))
    .pipe(cssnano())
    .pipe(gulp.dest('public/assets/css'));

});


////
// Libraries styles
////
gulp.task('libraries', function() {

  return sass('resources/scss/libraries.scss', { style: 'expanded' })
    .pipe(autoprefixer('last 2 version'))
    .pipe(gulp.dest('public/assets/css'))
    .pipe(rename({suffix: '.min'}))
    .pipe(cssnano())
    .pipe(gulp.dest('public/assets/css'));

});


////
// Scripts
////
gulp.task('scripts', function() {

  // make sure these are added in order of dependency
  return gulp.src([
    'node_modules/slick-carousel/slick/slick.min.js',
    'resources/frontend/theme/js/scripts.js'
  ])
    .pipe(jshint.reporter('default'))
    .pipe(rename('scripts.min.js'))
    .pipe(concat('scripts.min.js'))
    .pipe(uglify())
    .pipe(gulp.dest('public/assets/js'));
});


////
// Watcher task for CSS/Script compilers
////
gulp.task('watch', function() {

  // Watch main styles scss file
  gulp.watch([
    'resources/scss/style.scss',
    'resources/scss/partials/*'
  ], ['frontend-styles']);

  // Main libraries scss file
  gulp.watch([
    'resources/scss/libraries.scss',
    'node_modules/bootstrap/scss/*.scss',
    'node_modules/@fortawesome/fontawesome-free/scss/*.scss',
    'node_modules/slick-carousel/slick',
    'node_modules/slick-carousel/slick-theme'
  ], ['libraries']);

  // Scripts files
  gulp.watch([
    // worth splitting the libraries and the theme scripts in to separate files at some point
    'node_modules/slick-carousel/slick/slick.min.js',
    'resources/frontend/theme/js/scripts.js'
  ], ['scripts']);


});


////
// Clean before live
////
gulp.task('clean', function() {
  return del(['public/assets']);
});

////
//
////
gulp.task('default', ['clean'], function() {
  gulp.start('project-start', 'frontend-styles', 'libraries', 'scripts');
});