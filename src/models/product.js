let aDay = 24*60*60*1000

export default [
  {
    id: 1,
    name: 'Mouse mat',
    description: 'A mouse mat for all your smoother mouse using needs',
    amount_reserved: 40,
    reservation_goal: 50,
    price: 15,
    expiry_date: new Date().getTime() - aDay, // in the future
    image: 'https://static.scan.co.uk/images/products/542590-a.jpg',

  },
  {
    id: 2,
    name: 'Socks',
    description: 'when your feet lack warmth and colour',
    amount_reserved: 0,
    reservation_goal: 90,
    price: 17,
    expiry_date: new Date().getTime(), // only just expired
    image: 'http://3.bp.blogspot.com/-gYt41ue3DBs/UKPYlwBaSKI/AAAAAAAAdJo/t8lp7KryJaM/s1600/socks.jpg'
  },
  {
    id: 3,
    name: 'Shirts',
    description: '*see title*',
    amount_reserved: 122,
    reservation_goal: 150,
    price: 25,
    expiry_date: new Date().getTime() + aDay, // way in the past
    image: 'https://i3.cpcache.com/product/606802989/unicorn_kids_dark_tshirt.jpg'
  },
  {

    id: 4,
    name: 'Shirts 2',
    description: '*see title*',
    amount_reserved: 122,
    reservation_goal: 150,
    price: 25,
    expiry_date: new Date().getTime() + aDay, // way in the past
    image: 'https://i3.cpcache.com/product/606802989/unicorn_kids_dark_tshirt.jpg'
  },
  {
    id: 5,
    name: 'Socks 2',
    description: 'when your feet lack warmth and colour',
    amount_reserved: 25,
    reservation_goal: 90,
    price: 17,
    expiry_date: new Date().getTime() + aDay, // only just expired
    image: 'http://3.bp.blogspot.com/-gYt41ue3DBs/UKPYlwBaSKI/AAAAAAAAdJo/t8lp7KryJaM/s1600/socks.jpg'
  },
  {
    id: 6,
    name: 'Mouse mat 2',
    description: 'A mouse mat for all your smoother mouse using needs',
    amount_reserved: 176,
    reservation_goal: 50,
    price: 15,
    expiry_date: new Date().getTime() + aDay, // in the future
    image: 'https://static.scan.co.uk/images/products/542590-a.jpg',
  },
  {
    id: 7,
    name: 'Socks 3',
    description: 'when your feet lack warmth and colour',
    amount_reserved: 25,
    reservation_goal: 90,
    price: 17,
    expiry_date: new Date().getTime() - aDay, // only just expired
    image: 'http://3.bp.blogspot.com/-gYt41ue3DBs/UKPYlwBaSKI/AAAAAAAAdJo/t8lp7KryJaM/s1600/socks.jpg'
  },
  {

    id: 8,
    name: 'Shirts 3',
    description: '*see title*',
    amount_reserved: 122,
    reservation_goal: 150,
    price: 25,
    expiry_date: new Date().getTime() + aDay, // way in the past
    image: 'https://i3.cpcache.com/product/606802989/unicorn_kids_dark_tshirt.jpg'
  },
]