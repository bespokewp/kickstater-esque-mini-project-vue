import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from 'vuex-persistedstate'
import products from './models/product'

Vue.use(Vuex)

export default new Vuex.Store({
  plugins: [createPersistedState()],
  state: {

    count: 0,
    max_per_page: 6,
    statuses: {
      0: 'taking orders',
      1: 'expired',
      2: 'funded'
    },

    filters : [
      'id',
      'asc'
    ],

    // test data
    basket: {},
    products: products,

  },
  mutations: {
    /**
     * Add a product tot he basket based on ID and quantity in the payload
     * @param state
     * @param payload
     */
    addToBasket: function( state, payload ) {


      // update the basket
      let product_id  = typeof payload[0] !== 'undefined' ? payload[0] : 0;
      let quantity    = typeof payload[1] !== 'undefined' ? parseInt( payload[1] ) : 0;

      // if information is missing, kill it
      if ( product_id === 0 || quantity === 0 )
        return

      if (
        quantity > 0
        && quantity % 1 == 0
      ) {

        if ( typeof state.basket[product_id] !== "undefined" )
          quantity += state.basket[product_id]


        Vue.set( state.basket, product_id, quantity )

      }




    },

    /**
     *
     * Update a product in the basket with the reqiured quantity, or delete if quantity is zero
     * @param state
     * @param payload
     */
    updateBasket: function( state, payload ) {

      // update the basket
      let product_id  = parseInt( payload[0] )
      let quantity    = parseInt( payload[1] )


      if (
        typeof state.basket[product_id] !== "undefined"
        && quantity > 0
        && quantity % 1 == 0
      ) {
        Vue.set( state.basket, product_id, quantity )
      }
    },

    /**
     * Remove item from basket, if exists
     * @param state
     * @param product_id
     */
    removeFromBasket: function( state, product_id ) {
      if (
        typeof state.basket[product_id] !== "undefined"
      ) {
        Vue.delete(state.basket, product_id)
      }
    },

    /**
     * Apply new catalogue filters to the global object
     * @param state
     * @param payload
     */
    updateFilters: function( state, payload ) {

      let sort_by = payload[0]
      let sort_order = payload[1]

      Vue.set(state.filters, 0, sort_by)
      Vue.set(state.filters, 1, sort_order)

      // check to see if the property name actually exists in the objects first
      if ( typeof state.products[0][sort_by] !== 'undefined') {


        if ( sort_order.toLowerCase() === 'asc' ) {

          state.products.sort((a,b) => (a[sort_by] > b[sort_by] ) ? 1 : ((b[sort_by] > a[sort_by] ) ? -1 : 0)
          );

        } else {
          state.products.sort((a,b) => (a[sort_by] < b[sort_by] ) ? 1 : ((b[sort_by] < a[sort_by] ) ? -1 : 0));
        }

      }

    },

    /**
     * 'Checkout' all items in cart and then updated each items 'amount_reserved'
     * based on the quantity of that item in the basket
     * @param state
     */
    processCart : function( state ) {

      // setup
      let cart = state.basket
      let cart_keys = Object.keys(state.basket)
      let products = state.products


      // for each product in the global store
      for ( let i = 0; i < products.length; i++ ) {

        let product_id = products[i].id

        // update the 'amount_reserved' if there is an item in the cart that matches
        if ( typeof cart[product_id] !== 'undefined' ) {
          products[i].amount_reserved += cart[product_id]
          Vue.set(state.products, i,   products[i])

        }

      }

      // delete all items in the basket
      for ( let i = 0; i < cart_keys.length; i++ ) {
        Vue.delete(state.basket, cart_keys[i] )
      }

    },

    /**
     * Add new item to the global products store
     * @param state
     * @param payload
     */
    adminAddItem : function( state, payload ) {

      // Add data not parsed from form
      payload.id = state.products.length + 1
      payload.amount_reserved = 0

      state.products.push( payload )

    }
  }
})