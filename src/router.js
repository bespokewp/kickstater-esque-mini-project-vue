import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'root',
      redirect: '/products'
    },

    // single product page
    {
      path: '/products/page/:page_number(\\d+)',
      name: 'catalogue_products_paged',
      props: true,

      // route level code-splitting
      // this generates a separate chunk (<ROUTE_NAME>.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "catalogue_products_paged" */ './views/public/Catalogue.vue')

    },

    {
      path: '/products/',
      alias: '/',
      name: 'products',
      component: () => import(/* webpackChunkName: "products" */ './views/public/Catalogue.vue')
    },

    {
      path: '/products/:id(\\d+)',
      alias: '/',
      name: 'single_product',
      props: true,
      component: () => import(/* webpackChunkName: "products" */ './views/public/SingleProduct.vue')
    },

    {
      path: '/cart',
      name: 'cart',
      component: () => import(/* webpackChunkName: "cart" */ './views/public/Cart.vue')
    },

    {
      path: '/admin',
      name: 'admin',
      component: () => import(/* webpackChunkName: "about" */ './views/admin/Admin.vue')
    }
  ]
})
