import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

Vue.config.productionTip = false

// All of these are available directly in all components
Vue.mixin({

  // use these to call the properties in the store
  computed: {
    basket () {
      return store.state.basket
    },
    products () {
      return store.state.products
    }
  },
  methods : {
    // Return a product from the global store based on the ID
    productById : function( state, product_id ) {

      for ( let i = 0; i < state.products.length; i++ ) {
      if ( state.products[i].id ==product_id ) {
        return state.products[i]

        }
      }

    },
    // return the percentage amount of the total reserved
    reservedPercentage : function( product ) {
      let reserved = product.amount_reserved
      let required = product.reservation_goal
      return  ((reserved/required)*100) > 100 ? 100 : Math.floor((reserved/required)*100)
    },
    // return the percentage amount of the total reserved
    isInDate : function( product ) {
      var today  = new Date();
      var expiry_date  = new Date( product.expiry_date );

      return today < expiry_date
    }
  }
})

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
