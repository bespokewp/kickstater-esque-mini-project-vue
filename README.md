# Setup: Kickstarter Lite 



**Environment**
```
Node: v10.13.0 
NPM: V6.7.0
```

**1. Project setup**
```
npm install
```

**2. Compile CSS and JS libraries** 
```
gulp default
```

**3. Run the enviroment and follow the link given in the terminal**
```
npm run serve
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
